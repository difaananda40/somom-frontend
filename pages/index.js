import Head from 'next/head'
import {
  Section1, Section2, Section3, Section4, Section5, Section6,
  Section7, Section8, Section9, Section10, Section11, Section12,
} from '../components/Home'

export default function Home() {
  return (
    <>
      <Head>
        <title>Somom</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <Section1 />
      <Section2 />
      <Section3 />
      <Section4 />
      <Section5 />
      <Section6 />
      <Section7 />
      <Section8 />
      <Section9 />
      <Section10 />
      <Section11 />
      <Section12 />
    </>
  )
}
