import React from 'react';
import Navbar from '../Navbar'

const Layout = ({ children }) => (
  <React.Fragment>
    <Navbar />
    <main className="relative">
      {children}
    </main>
  </React.Fragment>
)

export default Layout