import PropTypes from 'prop-types';
import styles from './button.module.css';
import { useCallback } from 'react';

const Button = ({ type, children, className }) => {
  
  const typeClass = useCallback(() => {
    switch(type) {
      case 'primary': return styles.primary;
      case 'secondary': return styles.secondary;
      case 'outline-colored': return styles.outlineColored;
      case 'outline': return styles.outline;
      default: return styles.primary;
    }
  }, [type]) 

  return (
    <button type="button" className={typeClass() + ' ' + className}>
      {children}
    </button>
  )
}

Button.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node.isRequired,
  className: PropTypes.string
}

Button.defaultProps = {
  type: 'primary'
}

export default Button