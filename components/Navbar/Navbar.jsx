import React, { useState, useEffect } from 'react';

const Navbar = ({ children }) => {

  const [isToggled, setIsToggled] = useState(false);
  const toggle = () => setIsToggled(prev => !prev);

  const [ isScrolled, setIsScrolled ] = useState(false)
  
  useEffect(() => {
    const handleScroll = () => {
      const currentScroll = window.scrollY;
      const navHeight = 56;
      if(currentScroll > navHeight) {
        setIsScrolled(true)
      } else setIsScrolled(false)
    }

    const handleResize = () => setIsToggled(false);

    window.addEventListener('scroll', handleScroll)
    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('scroll', handleScroll)
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  const style = {
    style1: 'bg-transparent absolute w-full z-50 text-white',
    style2: 'bg-white fixed animate-fadeInDown w-full z-50 text-gray-900 shadow-md'
  }

  return (
    <nav className={!isScrolled && !isToggled ? style.style1 : style.style2}>
      <div className="mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-16">
          <div className="absolute inset-y-0 left-0 flex items-center lg:hidden">
            {/* Mobile menu button*/}
          
            <button className={`inline-flex items-center justify-center p-2 rounded-md ${!isScrolled ? 'text-white': 'text-gray-700'} hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white ${isToggled  && 'text-gray-700'}`} aria-expanded="false" onClick={toggle}>
              <span className="sr-only">Open main menu</span>
              {/* Icon when menu is closed. */}
              {/*
                Heroicon name: menu

                Menu open: "hidden", Menu closed: "block"
              */}
              <svg className="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
              {/* Icon when menu is open. */}
              {/*
                Heroicon name: x

                Menu open: "block", Menu closed: "hidden"
              */}
              <svg className="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div className="flex-1 flex items-center justify-center lg:items-stretch lg:justify-start">
            <div className="flex-shrink-0 flex items-center">
              {/* <img className="block lg:hidden h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg" alt="Workflow" />
              <img className="hidden lg:block h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg" alt="Workflow" /> */}
              {!isScrolled && !isToggled ? (
                <img src="./images/logo-light.svg" className="block w-auto" />
              ) : (
                <img src="./images/logo-dark.svg" className="block w-auto" />
              )}
            </div>
            <div className="hidden lg:block lg:ml-6">
              <div className="flex space-x-4 items-center justify-center">
                {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Solutions</a>
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Team</a>
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Partners</a>
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Insights</a>
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Contact</a>
                <a href="#" className="hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-bold uppercase">Support</a>
              </div>
            </div>
          </div>
          <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
            {/* <button className="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
              <span className="sr-only">View notifications</span>
              <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
              </svg>
            </button> */}
            <button type="button" className="bg-yellowish uppercase transition-all text-black text-sm font-bold px-5 py-3 rounded focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">Book <span className="hidden md:inline">a demo</span></button>
          </div>
        </div>
      </div>

      {/*
        Mobile menu, toggle classNamees based on menu state.

        Menu open: "block", Menu closed: "hidden"
      */}
      <div className={`${isToggled ? 'block' : 'hidden'}`}>
        <div className="bg-white px-2 pt-2 pb-3 space-y-1">
          {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Solutions</a>
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Team</a>
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Partners</a>
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Insights</a>
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Contact</a>
          <a href="#" className="text-gray-900 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-bold uppercase">Support</a>
        </div>
      </div>
    </nav>
  )
}

export default Navbar