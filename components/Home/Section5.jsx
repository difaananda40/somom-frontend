import styles from './styles/section5.module.css';

const Section5 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>Platform Features</h1>
    <div className={styles.containerSecond}>
      <div className={styles.row}>
        <div className={styles.col}>
          <img src="./icons/atom.svg" className={styles.icon} />
          <h2 className={styles.title}>Single unified compliance platform</h2>
          <p className={styles.caption}>Somom compliance ’s single unified platform integrates multiple compliance solutions into one master dashboard.</p>
        </div>
        <div className={styles.col}>
          <img src="./icons/box-empty.svg" className={styles.icon} />
          <h2 className={styles.title}>Highly configurable and intuitive UI</h2>
          <p className={styles.caption}>Our easily configurable dashboard gives you a view relevant to your tasks, allowing you to spot at a glance where suspicious activity may be occurring.</p>
        </div>
      </div>

      <div className={styles.row}>
        <div className={styles.col}>
          <img src="./icons/access-point.svg" className={styles.icon} />
          <h2 className={styles.title}>Flexible deployment</h2>
          <p className={styles.caption}>Should you need to deploy on-premise or in the cloud, we work with you closely to deliver the right outcome for your organisation.</p>
        </div>
        <div className={styles.col}>
          <img src="./icons/activity.svg" className={styles.icon} />
          <h2 className={styles.title}>Automated reporting and intuitive workflow</h2>
          <p className={styles.caption}>Increased  team’s efficiency and outcomes with intuitive workflows.</p>
        </div>
      </div>

      <div className={styles.row}>
        <div className={styles.col}>
          <img src="./icons/maximize.svg" className={styles.icon} />
          <h2 className={styles.title}>Scalable from zero to infinity</h2>
          <p className={styles.caption}>Our highly scalable platform can scale to match organisations of all sizes and sophistication, from start-ups to large global organisations.</p>
        </div>
        <div className={styles.col}>
          <img src="./icons/anchor.svg" className={styles.icon} />
          <h2 className={styles.title}>Reduce false positives</h2>
          <p className={styles.caption}>Using the only automated accounting transactions monitoring solution, Somom compliance helps dramatically reduce real false positives and correctly identify more false negatives.</p>
        </div>
      </div>

      {/* Shapes */}
      <img src="./shapes/shape3.svg" className="absolute z-0 -bottom-10 -right-10" />
    </div>
  </div>
)

export default Section5