import styles from './styles/section7.module.css';

const Section7 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>Why Somom Compliance?</h1>
    <div className="space-y-10 relative">

      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/policy-focus.svg" className="w-16" />
          <h2>Policy focus</h2>
        </div>
        <p className={styles.paragraph}>We will work with you to ensure your organisation’s AML and risk policies are reflected in the outcomes that you get from our platform and that they exceed regulatory demands.</p>
      </div>

      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/flexible-deployment.svg" className="w-16" />
          <h2>Flexible deployment</h2>
        </div>
        <p className={styles.paragraph}>Should you need to deploy on-premise or in the cloud, we work with you closely to deliver the right outcome for your organisation.</p>
      </div>
      
      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/continuous-success.svg" className="w-16" />
          <h2>Continuous success</h2>
        </div>
        <p className={styles.paragraph}>For self managed service, we provide every customer with a dedicated Customer Success Manager and access to a same day support as we believe you deserve the best experience possible when using our software.</p>
      </div>

      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/security-as-priority.svg" className="w-16" />
          <h2>Security as priority</h2>
        </div>
        <p className={styles.paragraph}>We work closely with cyber security experts to implement a product and platform that is secure by design. With our in-depth defence strategy and commitment to holistic security, you can rest assured that you, and your data, are in safe hands.</p>
      </div>

      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/data-independence.svg" className="w-16" />
          <h2>Data independence</h2>
        </div>
        <p className={styles.paragraph}>We work with all 3rd party data vendors and can help you select the right provider for your organisation.</p>
      </div>

      <div className={styles.card}>
        <div className={styles.head}>
          <img src="./icons/unique-service-offering.svg" className="w-16" />
          <h2>Unique service offering</h2>
        </div>
        <p className={styles.paragraph}>We are the first service provider to offer a fully managed service merging into one offering expertise, compliance officers and compliance technology.</p>
      </div>

    </div>
  </div>
)

export default Section7