import styles from './styles/section4.module.css';

const Section4 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>The role of technology for managed compliance services</h1>
    <div>
      <h2 className={styles.title}>The importance of the human element in AML compliance</h2>
      <p className={styles.paragraphOne}>We will always need people at the centre of our AML systems; technology is not designed to replace human decision-making but to help those making the decisions, for example bringing forward smaller datasets to analyse and collating information from a wide range of sources for a more informed review.</p>
      <p className={styles.paragraphTwo}>With the changing role of technology, it is only natural that the human element must evolve too. As technological developments allow for more efficient systems to be created, particularly within Artificial Intelligence (AI).</p>
    </div>
    <div>
      <h2 className={styles.title}>Closing the operational gap</h2>
      <p className={styles.paragraphOne}>It's quite common to find organisations highly dependent on manual and subjective decision-making which is where illicit activity may slip through the gaps. This risk can be reduced by building and maintaining sustainable and high-performing teams and strategizing the set-up of operational teams to leverage new technology. This is only possible if organisations can find suitable personnel to operate such technology.</p>
      <p className={styles.paragraphTwo}>Effective regulatory technology has a key role to play in building sustainable and high-performing teams. By removing greater amounts of mundane tasks from the operational process, compliance teams can focus on high-value and high-risk analysis. This not only reduces the risks of illicit activity being unidentified, but also increases the focus, performance and motivation of individuals within this control process.</p>
    </div>
  </div>
)

export default Section4