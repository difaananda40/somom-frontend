import styles from './styles/section1.module.css';

const Section1 = () => (
  <div
    className={styles.container}>
    <h1 className={styles.heading}>
      Managed and <br/>
      Self Managed <br/>
      Compliance services
    </h1>
    <p className={styles.caption}>
      Compliance services that are transforming <br/>
      how you firms stay compliant with <br/>
      asignificantly reduced  burden 
    </p>

    {/* Shapes */}
    <img src="./shapes/shape1.svg" className="absolute z-0 left-0" />
    <img src="./shapes/shape2.svg" className="absolute z-0 right-20 bottom-20" />
  </div>
)

export default Section1