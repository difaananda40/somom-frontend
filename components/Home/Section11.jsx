import styles from './styles/section11.module.css';

import Button from '../Button';

const Section11 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>
      Learn about technology,<br/>
      compliance & more
    </h1>
    <p className={styles.caption}>We’ll email you the latest industry insight.</p>
    <input type="text" className={styles.input} placeholder="Your name" />
    <input type="text" className={styles.input} placeholder="Your email" />
    <Button className="uppercase" type="secondary">Learn more</Button>
  </div>
)

export default Section11