import styles from './styles/section12.module.css';

const Section12 = () => (
  <div className={styles.container}>
    <div className={styles.card}>
      <h1 className={styles.cardTitle}>Get in touch</h1>
      <ul className={styles.cardList}>
        <li>21, Mons. A. Bonnici street</li>
        <li>info@somom.com</li>
      </ul>
    </div>
    <div className={styles.card}>
      <h1 className={styles.cardTitle}>Follow us</h1>
      <ul className={styles.cardList}>
        <li>LinkedIn</li>
        <li>Twitter</li>
      </ul>
    </div>
    <div className={styles.card}>
      <h1 className={styles.cardTitle}>Legal</h1>
      <ul className={styles.cardList}>
        <li>Terms and Conditions</li>
        <li>Privacy Policy</li>
      </ul>
    </div>
  </div>
)

export default Section12