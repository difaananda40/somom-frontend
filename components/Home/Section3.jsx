import styles from './styles/section3.module.css';

import Button from '../Button';

const Section3 = () => (
  <div className={styles.container}>
    <div className={styles.left}>
      <h1 className={styles.heading}>
        Supercharge your <br/>
        compliance
      </h1>
      
      <Button className="uppercase self-center lg:self-start mt-8 z-20" type="secondary">Book a demo</Button>

      <img src="./images/image1.png" className={styles.image} />
    </div>
    <div className={styles.right}>
      <p className={`mb-4 mt-8 lg:mt-0 ${styles.caption}`}>
        The Somom  platform  running  the compliance services is fast, scalable and easily configurable. It rapidly strengthens your AML defences and compliance capabilities, while meeting your compliance obligations and challenges in any sector.
      </p>
      <p className={styles.caption}>
        Our tools dramatically reduce both false positives and false negatives, empowering compliance teams to make validated decisions with unprecedented speed and accuracy.
      </p>
    </div>

    {/* Shapes */}
    <img src="./shapes/shape2.svg" className={styles.shape} />
  </div>
)

export default Section3