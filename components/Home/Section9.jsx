import styles from './styles/section9.module.css';

import Button from '../Button';

const Section9 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>Our Solutions</h1>
    <div className={styles.containerSecond}>
      <div className={styles.row}>
        <div className={styles.card}>
          <img src="./icons/alert-octagon.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Risk-Based due diligence</h2>
          <p className={styles.cardCaption}>Dramatically reduce false positives, while still detecting truly suspicious activity, with our machine learning-enabled transaction monitoring system designed to be used by non-technical business users in organisations of all sizes.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
        <div className={styles.card}>
          <img src="./icons/building.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Policies</h2>
          <p className={styles.cardCaption}>Run real-time searches on payment or transactional data against multiple watch and sanctions lists to make rapid decisions about the level of risk associated with payments beneficiaries and originators.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
        <div className={styles.card}>
          <img src="./icons/building-2.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Individuals Screening</h2>
          <p className={styles.cardCaption}>Screen individuals against designated sanction lists while leveraging a machine-learning programme and advanced fuzzy matching algorithms to reduce false positives.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
      </div>
      <div className={styles.row}>
        <div className={styles.card}>
          <img src="./icons/alien.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Client Activity Review</h2>
          <p className={styles.cardCaption}>Gain additional deeper insights about your customer’s activity with an automated ongoing review of all accounting record results - against their expected behaviour.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
        <div className={styles.card}>
          <img src="./icons/backhoe.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Customer risk assessments</h2>
          <p className={styles.cardCaption}>Perform enhanced risk assessments to generate a risk level for each client. Use these levels as part of a risk-based assessment to optimise your compliance processes.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
        <div className={styles.card}>
          <img src="./icons/adjustment.svg" className={styles.cardIcon} />
          <h2 className={styles.cardTitle}>Entity Screening</h2>
          <p className={styles.cardCaption}>Screen entities against designated sanctions and watch lists using advanced fuzzy matching algorithms to reduce false positives.</p>

          <Button className="uppercase" type="outline">Learn more</Button>
        </div>
      </div>
    </div>
  </div>
)

export default Section9