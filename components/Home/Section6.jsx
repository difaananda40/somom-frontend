import styles from './styles/section6.module.css';

const Section6 = () => (
  <div className={styles.container}>
    <div className={styles.containerSecond}>
      <div className={styles.left}>
        <img src="./images/image2.png" className={styles.image} />

        {/* Shapes */}
        <img src="./shapes/shape4.svg" className={styles.shape} />
      </div>
      <div className={styles.right}>
        <h1 className={styles.title}>About us</h1>
        <div className="space-y-8 ">
          <h2 className={styles.heading}>We are Somom Compliance</h2>
          <p className={styles.paragraph}>
            Somom is the world’s first end-to-end intelligent compliance platform. We want to change organisations’ attitudes to compliance, making it a proactive strength that gives businesses a competitive edge.
          </p>
          <p className={styles.paragraph}>
            We know we can make a real difference, by focusing on efficiency and outcomes. That’s why we have built an evolving suite of intelligent compliance products that aims to capture and share best practice across all sectors.
          </p>
          <p className={styles.paragraph}>
            By offering the most effective solutions for businesses, we can transform compliance.
          </p>
        </div>
      </div>
    </div>
  </div>
)

export default Section6