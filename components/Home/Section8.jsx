import styles from './styles/section8.module.css';
import Button from '../Button';

const Section8 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>Managed compliance <br/>services</h1>
    <p className={styles.caption}>
      Futureproof your operational efficiency by leveraging our <br/>managed compliance services. 
    </p>

    <Button className="uppercase self-start" type="outline-colored">Learn more</Button>

    <img src="./images/image3.png" className={styles.image} />
  </div>
)

export default Section8