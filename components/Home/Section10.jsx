import styles from './styles/section10.module.css';

import Button from '../Button';

const Section10 = () => (
  <div className={styles.container}>
    <h1 className={styles.heading}>It’s time. Let’s talk.</h1>
    <p className={styles.caption}>Book a demo and get critical insight into our unique approach,<br/>approved by specialist in compliance management.</p>
    <Button className="uppercase" type="secondary">Book a demo</Button>
  </div>
)

export default Section10