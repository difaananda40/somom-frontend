module.exports = {
  purge: ['./components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: [
        'Manrope',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: ['Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: ['Menlo', 'Monaco', 'Consolas', '"Liberation Mono"', '"Courier New"', 'monospace'],
    },
    extend: {
      keyframes: {
        fadeInDown: {
          'from': {opacity:0,transform:'translate3d(0, -100%, 0)'},
          'to': {opacity:1,transform:'none'}
        }
      },
      animation: {
        fadeInDown: '500ms ease-in-out 0s normal none 1 running fadeInDown'
      },
      colors: {
        yellowish: '#FFEA3B'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
